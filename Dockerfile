# build environment
FROM node:14.5.0-alpine as build

RUN apk update && apk add git

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./package.json /app/package.json

RUN npm install

COPY . .

RUN npm run build

# production environment
FROM nginx:1.16.0-alpine

ENV API_HOST=localhost
ENV API_PORT=8080

COPY --from=build /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY docker-entry.sh .
COPY nginx.conf.tmpl /tmp/nginx.conf.tmpl
EXPOSE 80
CMD ["/bin/sh", "docker-entry.sh"]
