import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Signin from '../views/Signin.vue'
import Apps from '../views/Apps.vue'
import DNS from '../views/DNS.vue'
import Kubernetes from '../views/Kubernetes.vue'
import WebProxy from '../views/WebProxy.vue'
import Databases from '../views/Databases.vue'
import LogViewer from '../views/LogViewer.vue'
import AppDetail from '../views/AppDetail.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/signin',
    name: 'Signin',
    component: Signin,
    meta: {
      title: 'Login',
      undecorated: true
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Início',
    }
  },
  {
    path: "/apps",
    name: "Apps",
    component: Apps,
    meta: {
      title: 'Aplicações'
    }
  },
  {
    path: "/dns",
    name: "DNS",
    component: DNS,
    meta: {
      title: 'DNS'
    }
  },
  {
    path: '/kubernetes',
    name: 'Kubernetes',
    component: Kubernetes,
    meta: {
      title: 'Kubernetes'
    }
  },
  {
    path: '/webproxy',
    name: 'WebProxy',
    component: WebProxy,
    meta: {
      title: 'Proxy Reverso'
    }
  },
  {
    path: '/databases',
    name: 'Databases',
    component: Databases,
    meta: {
      title: 'Bancos de Dados'
    }
  },
  {
    path: '/apps/:appname',
    name: 'AppDetail',
    component: AppDetail,
    meta: {
      cangoback: true,
      action_buttons_enabled: true,
    }
  },
  {
    path:'/apps/:appname/logs',
    name: 'LogViewer',
    component: LogViewer,
    meta: {
      undecorated: true,
    }
  },

  {
    path: '/about',
    name: 'About',
    meta: {
      title: 'Sobre'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((t, f, n) => {

  var account = JSON.parse(localStorage.getItem("account"))

  console.log("Current Account: " + account);
  console.log("going to: " + t.path)

  if (t.path == "/signin") {
    n();
  } else {

    if (account == undefined) {
      n("/signin")
    } else {
      n()
    }
  }
})

export default router
