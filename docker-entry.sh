#!/bin/sh

/usr/local/bin/envsubst '${API_HOST} ${API_PORT}' < /tmp/nginx.conf.tmpl > /etc/nginx/conf.d/nginx.conf;
nginx -g 'daemon off;'
